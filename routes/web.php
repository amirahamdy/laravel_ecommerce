<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//Dashboard
//login
Route::get('admin', 'loginController@adminIndex')->name('admin.login');
Route::post('admin', 'loginController@adminPosted');

Route::group(['middleware' => 'admin'], function(){

 
    Route::get("/admin_panel", 'admin_panel\dashboardController@index')->name('admin.dashboard');

    Route::get('admin/logout', 'loginController@adminLogout')->name('admin.logout');
    //categories
    Route::get('/admin_panel/categories', 'admin_panel\categoriesController@index')->name('admin.categories');
    Route::post('/admin_panel/categories', 'admin_panel\categoriesController@posted');

    Route::get('/admin_panel/categories/edit/{id}', 'admin_panel\categoriesController@edit')->name('admin.categories.edit');
    Route::post('/admin_panel/categories/edit/{id}', 'admin_panel\categoriesController@update');

    Route::get('/admin_panel/categories/delete/{id}', 'admin_panel\categoriesController@delete')->name('admin.categories.delete');
    Route::post('/admin_panel/categories/delete/{id}', 'admin_panel\categoriesController@destroy');


    //products
    Route::get('/admin_panel/products', 'admin_panel\productsController@index')->name('admin.products');

    Route::get('/admin_panel/products/create', 'admin_panel\productsController@create')->name('admin.products.create');
    Route::post('/admin_panel/products/create', 'admin_panel\productsController@store');

    Route::get('/admin_panel/products/edit/{id}', 'admin_panel\productsController@edit')->name('admin.products.edit');
    Route::post('/admin_panel/products/edit/{id}', 'admin_panel\productsController@update');

    Route::get('/admin_panel/products/delete/{id}', 'admin_panel\productsController@delete')->name('admin.products.delete');
    Route::post('/admin_panel/products/delete/{id}', 'admin_panel\productsController@destroy');

    //order management 
    Route::get('/admin_panel/management', 'admin_panel\managementController@manage')->name('admin.orderManagement');
    Route::post('/admin_panel/management', 'admin_panel\managementController@update')->name('admin.orderUpdate');


   // admin.PromoCodeManagement

    Route::get('/admin_panel/PromoCodes', 'admin_panel\PromoCodesController@index')->name('admin.PromoCodes');
    Route::post('/admin_panel/PromoCodes', 'admin_panel\PromoCodesController@posted');

    Route::get('/admin_panel/PromoCodes/edit/{id}', 'admin_panel\PromoCodesController@edit')->name('admin.PromoCodes.edit');
    Route::post('/admin_panel/PromoCodes/edit/{id}', 'admin_panel\PromoCodesController@update');

    Route::get('/admin_panel/PromoCodes/delete/{id}', 'admin_panel\PromoCodesController@delete')->name('admin.PromoCodes.delete');
    Route::post('/admin_panel/PromoCodes/delete/{id}', 'admin_panel\PromoCodesController@destroy');


});

Route::get('/login', 'loginController@userIndex')->name('user.login');
Route::post('/login', 'loginController@userPosted');

//signup
Route::get('/signup', 'signupController@userIndex')->name('user.signup');
Route::post('/signup', 'signupController@userPosted');
Route::post('/check_email', 'signupController@emailCheck')->name('user.signup.check_email');


//user
Route::get('/', 'user\userController@index')->name('user.home');
Route::get('/product/{id}', 'user\userController@view')->name('user.product');

Route::get('/search', 'user\userController@search')->name('user.search');
Route::get('/search?c={id}', 'user\userController@view')->name('user.search.cat');



Route::get('/view/{id}', 'user\userController@view')->name('user.view');
Route::post('/view/{id}', 'user\userController@post');

// to rate product
Route::get('/rate/{id}', 'user\userController@view_rate_product')->name('user.review');
Route::post('/rate/{id}', 'user\userController@rate_product');


Route::get('/cart', 'user\userController@cart')->name('user.cart');
Route::post('/cart', 'user\userController@confirm');

Route::post('/edit_cart', 'user\userController@editCart')->name('user.editCart');
//Route::post('/delete_item_from_cart', 'user\userController@deleteCartItem')->name('user.deleteCartItem');
Route::get('/delete-user/{pro_id}','user\userController@deleteCartItem'); //delete user


//user.ApplyPromoCode

Route::get('/ApplyPromoCode', 'user\userController@apply_promocode')->name('user.ApplyPromoCode');
Route::post('/ApplyPromoCodeSave', 'user\userController@apply_promocode_confirm');



Route::get('/logout', 'loginController@userLogout')->name('user.logout');

Route::group(['middleware' => 'user'], function(){
Route::get('/history', 'user\userController@history')->name('user.history');


Route::get('/order/confirmed',function(){
    return view('emails.success');
  });
  

});


/****************EXport EXCEL Sheet Links */
Route::get('/admin/export/product/excel', 'admin_panel\productsController@export_products_excel');

Route::get('/admin/export/customers/excel', 'admin_panel\managementController@export_customers_excel');

/***************arabic pages */


Route::get('/about-us', 'user\userController@about_us');
Route::get('/order-returns', 'user\userController@order_returns');
Route::get('/terms-conditions', 'user\userController@terms_conditions');

