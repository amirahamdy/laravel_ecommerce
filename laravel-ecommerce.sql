-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Apr 02, 2021 at 05:36 AM
-- Server version: 10.4.17-MariaDB
-- PHP Version: 7.4.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `laravel-ecommerce`
--

-- --------------------------------------------------------

--
-- Table structure for table `addresses`
--

CREATE TABLE `addresses` (
  `id` int(10) UNSIGNED NOT NULL,
  `area` text NOT NULL,
  `city` text NOT NULL,
  `zip` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `addresses`
--

INSERT INTO `addresses` (`id`, `area`, `city`, `zip`, `created_at`, `updated_at`) VALUES
(1, 'test', 'alex', 1234, '2021-03-13 01:02:08', '2021-03-13 01:02:08'),
(2, 'test', 'alex', 1234, '2021-04-01 20:32:12', '2021-04-01 20:32:12');

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `username`, `password`, `name`, `created_at`, `updated_at`) VALUES
(1, 'admin', '12345', 'Vera', '2018-08-27 22:00:00', '2018-08-27 22:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `cart`
--

CREATE TABLE `cart` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `product_id` text NOT NULL,
  `cart_status` text DEFAULT NULL,
  `price` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cart`
--

INSERT INTO `cart` (`id`, `user_id`, `product_id`, `cart_status`, `price`, `created_at`, `updated_at`) VALUES
(33, 1, '596:1:#000000:1', '0', NULL, '2021-04-01 22:23:33', '2021-04-01 22:23:33');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `type` text NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `type`, `created_at`, `updated_at`) VALUES
(11, 'Discount Products', 'All Discount Products', '2021-03-11 08:04:30', '2021-03-11 08:04:30'),
(12, 'All', 'All Products', '2021-03-12 22:48:58', '2021-03-12 22:48:58');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2020_04_19_173215_create_admins_table', 1),
(2, '2020_04_19_175655_create_addresses_table', 1),
(3, '2020_04_19_175938_create_categories_table', 1),
(4, '2020_04_20_141633_create_products_table', 1),
(5, '2020_04_20_142309_create_users_table', 1),
(6, '2020_04_20_142632_create_sales_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(100) NOT NULL,
  `serial_id` text DEFAULT NULL,
  `name` text DEFAULT NULL,
  `description` text DEFAULT NULL,
  `description_arabic` text DEFAULT NULL,
  `colors` varchar(100) DEFAULT NULL,
  `price` int(100) DEFAULT NULL,
  `discount` int(100) DEFAULT NULL,
  `original_stock` int(100) DEFAULT NULL,
  `stock` int(100) DEFAULT NULL,
  `min_stock` int(100) DEFAULT NULL,
  `tag` text DEFAULT NULL,
  `category_id` int(10) DEFAULT NULL,
  `main_image` text DEFAULT NULL,
  `image_name` longtext DEFAULT NULL,
  `rate` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `serial_id`, `name`, `description`, `description_arabic`, `colors`, `price`, `discount`, `original_stock`, `stock`, `min_stock`, `tag`, `category_id`, `main_image`, `image_name`, `rate`, `created_at`, `updated_at`) VALUES
(1, NULL, 'Adapter AC-AC 220VAC TO 110ACV10 WATT', 'Adapter from AC 220 to AC 11010 watts', NULL, ',#000000', 100, 95, 10, 8, 1, 'ijsdukhghjsdg', 12, NULL, '[\"addproduct_details.png\",\"user_1.png\",\"user_2.png\",\"user_addcart.png\",\"user_confirmorder.png\"]', NULL, NULL, '2021-03-21 11:55:54'),
(2, NULL, 'Adapter AC-AC 220VAC TO 110ACV30 WATT', 'Adapter from AC 220 to AC 11030 watts', NULL, NULL, 150, NULL, 10, 10, 1, NULL, 12, NULL, NULL, NULL, NULL, NULL),
(3, NULL, 'Adapter AC-AC 220VAC TO 110ACV50 WATT', 'Adapter from AC 220 to AC 11050 WATT', NULL, NULL, 250, NULL, 10, 10, 1, NULL, 12, NULL, NULL, NULL, NULL, NULL),
(4, NULL, 'Adapter 12V 1A', 'Input AC 100-240 VAC50 - 60 MHzOutput 12 VDC 1 A', NULL, NULL, 25, NULL, 50, 50, 1, NULL, 12, NULL, '', NULL, NULL, NULL),
(5, NULL, 'Adapter 12V 2A', 'Input AC 100-240 VAC50 - 60 MHzOutput 12 VDC 2 A', NULL, NULL, 35, NULL, 50, 50, 1, NULL, 12, NULL, '', NULL, NULL, NULL),
(6, NULL, 'Adapter 12V 3A', 'Input AC 100-240 VAC50 - 60 MHzOutput 12 VDC 3 A', NULL, NULL, 45, NULL, 50, 50, 1, NULL, 12, NULL, '', NULL, NULL, NULL),
(7, NULL, 'Adapter 12V 5A', 'Input AC 100-240 VAC50 - 60 MHzOutput 12 VDC 5 A', NULL, NULL, 65, NULL, 50, 50, 1, NULL, 12, NULL, '', NULL, NULL, NULL),
(8, NULL, 'Adapter 3V 2A', 'Input AC 100-240 VAC50 - 60 MHzOutput 3VDC 2A', NULL, NULL, 35, NULL, 50, 50, 1, NULL, 12, NULL, '', NULL, NULL, NULL),
(9, NULL, 'Adapter 5V 1A', 'Input AC 100-240 VAC50 - 60 MHzOutput 5 VDC 1A', NULL, NULL, 25, NULL, 50, 50, 1, NULL, 12, NULL, '', NULL, NULL, NULL),
(10, NULL, 'Adapter 5V 2A', 'Input AC 100-240 VAC50 - 60 MHzOutput 5 VDC 2 A', NULL, NULL, 35, NULL, 50, 50, 1, NULL, 12, NULL, '', NULL, NULL, NULL),
(11, NULL, 'Adapter 9V 1A', 'Input AC 100-240 VAC50 - 60 MHzOutput 9 VDC 1 A', NULL, NULL, 25, NULL, 5, 5, 1, NULL, 12, NULL, '', NULL, NULL, NULL),
(12, NULL, 'Adapter 9V 4A', 'Input AC 100-240 VAC50 - 60 MHzOutput 9 VDC 4 A', NULL, NULL, 95, NULL, 5, 5, 1, NULL, 12, NULL, NULL, NULL, NULL, NULL),
(13, NULL, 'Adapter 6V 2A', 'Input AC 100-240 VAC50 - 60 MHzOutput 6 VDC 2 A', NULL, NULL, 35, NULL, 5, 5, 1, NULL, 12, NULL, '', NULL, NULL, NULL),
(14, NULL, 'Adapter 42V 2A', 'Input AC 100-240 VAC50 - 60 MHzOutput 24 VDC 2 A', NULL, NULL, 55, NULL, 5, 5, 1, NULL, 12, NULL, '', NULL, NULL, NULL),
(15, NULL, 'Adapter 24V 2A', 'Input AC 100-240 VAC50 - 60 MHzOutput 24 VDC 5 A', NULL, NULL, 150, NULL, 5, 5, 1, NULL, 12, NULL, '', NULL, NULL, NULL),
(16, NULL, 'Adapter 20V 2A', 'Input AC 100-240 VAC50 - 60 MHzOutput 20 VDC 2 A', NULL, NULL, 55, NULL, 5, 5, 1, NULL, 12, NULL, '', NULL, NULL, NULL),
(17, NULL, 'Adapter 18V 2A', 'Input AC 100-240 VAC50 - 60 MHzOutput 18 VDC 2 A', NULL, NULL, 55, NULL, 5, 5, 1, NULL, 12, NULL, '', NULL, NULL, NULL),
(18, NULL, 'Adapter 1.5,3,4.5,6,9,12 DC.V300MA', 'Input AC 100-240 VAC50 - 60 MHzOutput 1.5,3,4.5,6,9,12 V DC', NULL, NULL, 45, NULL, 25, 25, 1, NULL, 12, NULL, '', NULL, NULL, NULL),
(19, NULL, 'Adapter 1.5,3,4.5,6,9,12 DC.V500MA', 'Input AC 100-240 VAC50 - 60 MHzOutput 1.5,3,4.5,6,9,12 V DC', NULL, NULL, 45, NULL, 25, 25, 1, NULL, 12, NULL, '', NULL, NULL, NULL),
(20, NULL, 'Adapter 1.5,3,4.5,6,9,12 DC.V1000MA', 'Input AC 100-240 VAC50 - 60 MHzOutput 1.5,3,4.5,6,9,12 V DC', NULL, NULL, 55, NULL, 25, 25, 1, NULL, 12, NULL, '', NULL, NULL, NULL),
(21, NULL, 'Power Supply 12V 5A', 'Input AC 100-240 VAC50 - 60 MHzOutput 12 VDC 5 A', NULL, NULL, 55, NULL, 25, 25, 1, NULL, 12, NULL, '', NULL, NULL, NULL),
(22, NULL, 'Power Supply 12V 10ASLIM', 'Input AC 100-240 VAC50 - 60 MHzOutput 12 VDC 10 A', NULL, NULL, 125, NULL, 25, 25, 1, NULL, 12, NULL, NULL, NULL, NULL, NULL),
(23, NULL, 'Power Supply 12V 20ASLIM', 'Input AC 100-240 VAC50 - 60 MHzOutput 12 VDC 20A', NULL, NULL, 150, NULL, 25, 25, 1, NULL, 12, NULL, NULL, NULL, NULL, NULL),
(24, NULL, 'Power Supply 12V 30ASLIM', 'Input AC 100-240 VAC50 - 60 MHzOutput 12 VDC30 A', NULL, NULL, 225, NULL, 25, 25, 1, NULL, 12, NULL, NULL, NULL, NULL, NULL),
(25, NULL, 'Power Supply 12V 10A', 'Input AC 100-240 VAC50 - 60 MHzOutput 12 VDC 10 A', NULL, NULL, 85, NULL, 25, 25, 1, NULL, 12, NULL, NULL, NULL, NULL, NULL),
(26, NULL, 'Power Supply 12V 20A', 'Input AC 100-240 VAC50 - 60 MHzOutput 12 VDC 20A', NULL, NULL, 125, NULL, 25, 25, 1, NULL, 12, NULL, NULL, NULL, NULL, NULL),
(27, NULL, 'Power Supply 12V 30A', 'Input AC 100-240 VAC50 - 60 MHzOutput 12 VDC30 A', NULL, NULL, 190, NULL, 25, 25, 1, NULL, 12, NULL, NULL, NULL, NULL, NULL),
(28, NULL, 'Power Supply 24V DC 10A', 'Input AC 100-240 VAC50 - 60 MHzOutput 24 VDC 10 A', NULL, NULL, 150, NULL, 25, 25, 1, NULL, 12, NULL, NULL, NULL, NULL, NULL),
(29, NULL, 'Power Supply 24V DC 20A', 'Input AC 100-240 VAC50 - 60 MHzOutput 24 VDC 20 A', NULL, NULL, 190, NULL, 25, 25, 1, NULL, 12, NULL, NULL, NULL, NULL, NULL),
(30, NULL, 'Power Supply 24V DC30A', 'Input AC 100-240 VAC50 - 60 MHzOutput 24 VDC 30 A', NULL, NULL, 250, NULL, 25, 25, 1, NULL, 12, NULL, NULL, NULL, NULL, NULL),
(31, NULL, 'Power Supply 24V DC 40A', 'Input AC 100-240 VAC50 - 60 MHzOutput 24 VDC40 A', NULL, NULL, 275, NULL, 25, 25, 1, NULL, 12, NULL, NULL, NULL, NULL, NULL),
(32, NULL, 'BTC 2 PIN', NULL, NULL, NULL, 5, NULL, 25, 25, 1, NULL, 12, NULL, '', NULL, NULL, NULL),
(33, NULL, 'BTC 3 PIN', NULL, NULL, NULL, 5, NULL, 25, 25, 1, NULL, 12, NULL, '', NULL, NULL, NULL),
(34, NULL, 'DIEOD 1A', NULL, NULL, NULL, 1, NULL, 25, 25, 1, NULL, 12, NULL, '', NULL, NULL, NULL),
(35, NULL, 'DIEOD 2A', NULL, NULL, NULL, 1, NULL, 25, 25, 1, NULL, 12, NULL, '', NULL, NULL, NULL),
(36, NULL, 'DIEOD 3A', NULL, NULL, NULL, 1, NULL, 25, 25, 1, NULL, 12, NULL, NULL, NULL, NULL, NULL),
(37, NULL, 'DIEOD 6A', NULL, NULL, NULL, 1, NULL, 25, 25, 1, NULL, 12, NULL, '', NULL, NULL, NULL),
(38, NULL, 'Baradge 1 A', NULL, NULL, NULL, 5, NULL, 25, 25, 1, NULL, 12, NULL, '', NULL, NULL, NULL),
(39, NULL, 'Baradge 3A', 'Bridge Rectifier Single Phase 3 Ampere – 1000V in comb Shape', NULL, NULL, 8, NULL, 25, 25, 1, NULL, 12, NULL, NULL, NULL, NULL, NULL),
(40, NULL, 'Baradge 6A', 'Bridge Rectifier Single Phase 6 Ampere – 1000V in comb Shape', NULL, NULL, 10, NULL, 25, 25, 1, NULL, 12, NULL, '', NULL, NULL, NULL),
(41, NULL, 'Baradge 8A', 'Bridge Rectifier Single Phase 8 Ampere – 1000V in comb Shape', NULL, NULL, 12, NULL, 25, 25, 1, NULL, 12, NULL, '', NULL, NULL, NULL),
(42, NULL, 'Baradge 35 A', 'Bridge Rectifier Single Phase 35 Ampere – 1000V in square Shape and terminals PCB solder', NULL, NULL, 15, NULL, 25, 25, 1, NULL, 12, NULL, '', NULL, NULL, NULL),
(43, NULL, 'Baradge 50 A', 'Bridge Rectifier Single Phase 50 Ampere – 1000V in square Shape and terminals PCB solder', NULL, NULL, 20, NULL, 25, 25, 1, NULL, 12, NULL, '', NULL, NULL, NULL),
(44, NULL, 'Baradge 35 A 3 Fase', 'Bridge Rectifier 3 Phase 35 Ampere – 1000V in square Shape and terminals PCB solder', NULL, NULL, 35, NULL, 25, 25, 1, NULL, 12, NULL, '', NULL, NULL, NULL),
(45, NULL, 'Baradge 50 A 3 Fase', 'Bridge Rectifier 3 Phase 50 Ampere – 1000V in square Shape and terminals PCB solder', NULL, NULL, 35, NULL, 25, 25, 1, NULL, 12, NULL, '', NULL, NULL, NULL),
(46, NULL, 'Inverter 2 way', NULL, NULL, NULL, 45, NULL, 25, 25, 1, NULL, 12, NULL, '', NULL, NULL, NULL),
(47, NULL, 'Inverter 4 way', NULL, NULL, NULL, 50, NULL, 25, 25, 1, NULL, 12, NULL, '', NULL, NULL, NULL),
(48, NULL, 'Inverter LED ', NULL, NULL, NULL, 80, NULL, 25, 25, 1, NULL, 12, NULL, '', NULL, NULL, NULL),
(49, NULL, 'Inverter LED Big size ', NULL, NULL, NULL, 120, NULL, 25, 25, 1, NULL, 12, NULL, NULL, NULL, NULL, NULL),
(50, NULL, 'Power 100 W', NULL, NULL, NULL, 25, NULL, 25, 25, 1, NULL, 12, NULL, '', NULL, NULL, NULL),
(51, NULL, 'Power 180 W', NULL, NULL, NULL, 30, NULL, 25, 25, 1, NULL, 12, NULL, '', NULL, NULL, NULL),
(52, NULL, 'Power 001', NULL, NULL, NULL, 20, NULL, 25, 25, 1, NULL, 12, NULL, '', NULL, NULL, NULL),
(53, NULL, 'Power 009', NULL, NULL, NULL, 25, NULL, 25, 25, 1, NULL, 12, NULL, '', NULL, NULL, NULL),
(54, NULL, 'Power 9023', NULL, NULL, NULL, 25, NULL, 25, 25, 1, NULL, 12, NULL, '', NULL, NULL, NULL),
(55, NULL, 'Power 5 Pins', NULL, NULL, NULL, 25, NULL, 25, 25, 1, NULL, 12, NULL, '', NULL, NULL, NULL),
(56, NULL, 'Power 5 Pins Big Size', NULL, NULL, NULL, 20, NULL, 25, 25, 1, NULL, 12, NULL, '', NULL, NULL, NULL),
(57, NULL, 'Soldring Tip For HotAir', NULL, NULL, NULL, 15, NULL, 25, 25, 1, NULL, 12, NULL, '', NULL, NULL, NULL),
(58, NULL, 'Soldring Tip For HotAir', NULL, NULL, NULL, 15, NULL, 25, 25, 1, NULL, 12, NULL, NULL, NULL, NULL, NULL),
(59, NULL, 'Soldring Tip For HotAir', NULL, NULL, NULL, 15, NULL, 25, 25, 1, NULL, 12, NULL, NULL, NULL, NULL, NULL),
(60, NULL, 'Soldring Tip 30 W', NULL, NULL, NULL, 10, NULL, 25, 25, 1, NULL, 12, NULL, NULL, NULL, NULL, NULL),
(61, NULL, 'Soldring Tip 40 W', NULL, NULL, NULL, 15, NULL, 25, 25, 1, NULL, 12, NULL, NULL, NULL, NULL, NULL),
(62, NULL, 'Soldring Tip 60W', NULL, NULL, NULL, 15, NULL, 25, 25, 1, NULL, 12, NULL, NULL, NULL, NULL, NULL),
(63, NULL, 'Soldring Tip 30 W', NULL, NULL, NULL, 15, NULL, 25, 25, 1, NULL, 12, NULL, NULL, NULL, NULL, NULL),
(64, NULL, 'Soldring Tip 40 W', NULL, NULL, NULL, 25, NULL, 25, 25, 1, NULL, 12, NULL, '', NULL, NULL, NULL),
(65, NULL, 'Soldring Tip 60W', NULL, NULL, NULL, 30, NULL, 25, 25, 1, NULL, 12, NULL, '', NULL, NULL, NULL),
(197, NULL, 'OPTICAL/FAI-AUDIO', NULL, NULL, NULL, 200, NULL, 25, 25, 1, NULL, 12, NULL, NULL, NULL, NULL, NULL),
(200, NULL, 'Transformer300MA', NULL, NULL, NULL, 20, NULL, 25, 25, 1, NULL, 12, NULL, NULL, NULL, NULL, NULL),
(201, NULL, 'Transformer 600MA', NULL, NULL, NULL, 30, NULL, 25, 25, 1, NULL, 12, NULL, NULL, NULL, NULL, NULL),
(202, NULL, 'Transformer 1000MA', NULL, NULL, NULL, 35, NULL, 25, 25, 1, NULL, 12, NULL, NULL, NULL, NULL, NULL),
(203, NULL, 'Transformer 3000MA', NULL, NULL, NULL, 55, NULL, 25, 25, 1, NULL, 12, NULL, NULL, NULL, NULL, NULL),
(204, NULL, 'Transformer 18-0-18-09', NULL, NULL, NULL, 55, NULL, 25, 25, 1, NULL, 12, NULL, NULL, NULL, NULL, NULL),
(208, NULL, 'Volume 10K', NULL, NULL, NULL, 5, NULL, 25, 25, 1, NULL, 12, NULL, NULL, NULL, NULL, NULL),
(209, NULL, 'Volume 20k', NULL, NULL, NULL, 5, NULL, 25, 25, 1, NULL, 12, NULL, NULL, NULL, NULL, NULL),
(210, NULL, 'Volume 50k', NULL, NULL, NULL, 5, NULL, 25, 25, 1, NULL, 12, NULL, NULL, NULL, NULL, NULL),
(211, NULL, 'Volume 1M', NULL, NULL, NULL, 5, NULL, 25, 25, 1, NULL, 12, NULL, '', NULL, NULL, NULL),
(212, NULL, 'Volume3+3 50k', NULL, NULL, NULL, 5, NULL, 25, 25, 1, NULL, 12, NULL, '', NULL, NULL, NULL),
(213, NULL, 'Volume3+3 100k', NULL, NULL, NULL, 5, NULL, 25, 25, 1, NULL, 12, NULL, '', NULL, NULL, NULL),
(214, NULL, 'BUZERR SMALL', NULL, NULL, NULL, 5, NULL, 25, 25, 1, NULL, 12, NULL, '', NULL, NULL, NULL),
(215, NULL, 'BUZERR BIG', NULL, NULL, NULL, 15, NULL, 25, 25, 1, NULL, 12, NULL, '', NULL, NULL, NULL),
(216, NULL, 'Rozeta 2 pin', NULL, NULL, NULL, 1, NULL, 25, 25, 1, NULL, 12, NULL, '', NULL, NULL, NULL),
(217, NULL, 'Rozeta 4 pin', NULL, NULL, NULL, 2, NULL, 25, 25, 1, NULL, 12, NULL, NULL, NULL, NULL, NULL),
(218, NULL, 'Relya 48V', NULL, NULL, NULL, 10, NULL, 25, 25, 1, NULL, 12, NULL, NULL, NULL, NULL, NULL),
(219, NULL, 'Relay 5V', NULL, NULL, NULL, 10, NULL, 25, 25, 1, NULL, 12, NULL, '', NULL, NULL, NULL),
(220, NULL, 'Relay 12V', NULL, NULL, NULL, 5, NULL, 25, 25, 1, NULL, 12, NULL, '', NULL, NULL, NULL),
(221, NULL, 'Relay 24V', NULL, NULL, NULL, 10, NULL, 25, 25, 1, NULL, 12, NULL, '', NULL, NULL, NULL),
(222, NULL, 'Relay 12V 8Pin', NULL, NULL, NULL, 25, NULL, 25, 25, 1, NULL, 12, NULL, '', NULL, NULL, NULL),
(223, NULL, 'Deap Switch 4pin', NULL, NULL, NULL, 4, NULL, 25, 25, 1, NULL, 12, NULL, '', NULL, NULL, NULL),
(224, NULL, 'Deap Switch 8pin', NULL, NULL, NULL, 8, NULL, 25, 25, 1, NULL, 12, NULL, '', NULL, NULL, NULL),
(225, NULL, 'L.D.R', NULL, NULL, NULL, 5, NULL, 25, 25, 1, NULL, 12, NULL, '', NULL, NULL, NULL),
(226, NULL, 'L.D.R.BIG', NULL, NULL, NULL, 8, NULL, 25, 25, 1, NULL, 12, NULL, NULL, NULL, NULL, NULL),
(227, NULL, 'Bin Heder', NULL, NULL, NULL, 5, NULL, 25, 25, 1, NULL, 12, NULL, '', NULL, NULL, NULL),
(228, NULL, 'Base 8 pin', NULL, NULL, NULL, 1, NULL, 25, 25, 1, NULL, 12, NULL, NULL, NULL, NULL, NULL),
(229, NULL, 'Base 16 pin', NULL, NULL, NULL, 1, NULL, 25, 25, 1, NULL, 12, NULL, NULL, NULL, NULL, NULL),
(230, NULL, 'Base 18 pin', NULL, NULL, NULL, 1, NULL, 25, 25, 1, NULL, 12, NULL, NULL, NULL, NULL, NULL),
(231, NULL, 'Base 24 pin', NULL, NULL, NULL, 2, NULL, 25, 25, 1, NULL, 12, NULL, NULL, NULL, NULL, NULL),
(232, NULL, 'Base 40 pin', NULL, NULL, NULL, 5, NULL, 25, 25, 1, NULL, 12, NULL, NULL, NULL, NULL, NULL),
(233, NULL, 'zef socket', NULL, NULL, NULL, 5, NULL, 25, 25, 1, NULL, 12, NULL, NULL, NULL, NULL, NULL),
(234, NULL, 'Battery House twins', NULL, NULL, NULL, 10, NULL, 25, 25, 1, NULL, 12, NULL, '', NULL, NULL, NULL),
(235, NULL, 'Battery Hous Twins with switch', NULL, NULL, NULL, 12, NULL, 25, 25, 1, NULL, 12, NULL, '', NULL, NULL, NULL),
(236, NULL, 'Battery House 4', NULL, NULL, NULL, 15, NULL, 25, 25, 1, NULL, 12, NULL, '', NULL, NULL, NULL),
(237, NULL, 'Battery House 9v', NULL, NULL, NULL, 15, NULL, 25, 25, 1, NULL, 12, NULL, NULL, NULL, NULL, NULL),
(238, NULL, 'Breadpoard', NULL, NULL, NULL, 5, NULL, 25, 25, 1, NULL, 12, NULL, '', NULL, NULL, NULL),
(239, NULL, 'Breadpoard', NULL, NULL, NULL, 10, NULL, 25, 25, 1, NULL, 12, NULL, '', NULL, NULL, NULL),
(592, '123', '123', 'jiuhghf', NULL, '#000000', 2000, 2000, 10, 10, 1, 'kkjhgfdfhjj', 11, NULL, '[\"addproduct_details.png\",\"addproduct_morepicture.png\",\"admin_1.png\",\"admin_2addcategory.png\",\"admin_3listproducts.png\",\"camera_56f176e3-ad83-4ff8-82d8-d53d71b6e0fe.jpg\",\"edit_delete product.png\",\"order_management.png\",\"photo-1505740420928-5e560c06d30e.jpeg\"]', NULL, '2021-03-18 09:49:40', '2021-03-18 09:49:40'),
(593, '123456', 'amira_main_photo', 'kjdhghfjzmxc', NULL, '#000000', 2000, 2000, 10, 10, 1, 'ijsdukhghjsdg', 11, '/tmp/phpIPCLq7', '[\"addproduct_details.png\",\"user_1.png\",\"user_2.png\",\"user_addcart.png\",\"user_confirmorder.png\"]', NULL, '2021-03-19 06:58:53', '2021-03-19 06:58:53'),
(594, '123456789', 'amira', 'ijhaygfjsd', NULL, '#000000', 2000, 1500, 10, 10, 1, 'ijsdukhghjsdg', 11, '/tmp/phpCI4DTC', '[\"addproduct_details.png\",\"Screenshot from 2021-03-16 16-02-52.png\",\"user_1.png\",\"user_2.png\",\"user_addcart.png\",\"user_confirmorder.png\"]', NULL, '2021-03-21 08:44:22', '2021-03-21 08:44:22'),
(595, '123545858779898989', 'mkjhgvcvb', 'lkjkhjjv', NULL, '#000000', 554, 454, 12, 12, 5, 'jho;lk;l', 11, '/tmp/php3t5zTD', '[\"addproduct_details.png\",\"user_1.png\",\"user_2.png\",\"user_addcart.png\",\"user_confirmorder.png\"]', NULL, '2021-03-21 08:59:07', '2021-03-21 08:59:07'),
(596, '555555555', 'test_main_image', 'klhjgfdfghh', NULL, '#000000', 2000, 1500, 10000, 9981, 4, 'jhjhfgn', 11, 'Product_Lg_Type.jpg', '[\"addproduct_details.png\",\"user_1.png\",\"user_2.png\",\"user_addcart.png\",\"user_confirmorder.png\"]', NULL, '2021-03-21 09:23:36', '2021-04-01 21:54:36'),
(597, '66666666', 'test_color_6666666666666', 'kjhgfdgh', NULL, '#000000', 2000, 1000, 8, 49, 4, 'poihujg', 11, 'Product.jpg', '[\"addproduct_details.png\",\"user_1.png\",\"user_2.png\",\"user_addcart.png\",\"user_confirmorder.png\"]', NULL, '2021-03-21 11:12:12', '2021-04-01 21:45:52'),
(598, '125448', 'test_arabic', 'test', 'نجرب العربى', '#000000', 2000, 1000, 10, 3, 1, 'poihujg', 11, 'product.jpg', '[\"addproduct_details.png\",\"user_1.png\",\"user_2.png\",\"user_addcart.png\",\"user_confirmorder.png\"]', NULL, '2021-03-30 10:30:58', '2021-04-01 21:02:22');

-- --------------------------------------------------------

--
-- Table structure for table `products_old`
--

CREATE TABLE `products_old` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `image_name` text NOT NULL,
  `description` text NOT NULL,
  `colors` text NOT NULL,
  `price` int(11) NOT NULL,
  `discount` int(11) NOT NULL,
  `original_stock` int(100) DEFAULT NULL,
  `stock` int(11) DEFAULT NULL,
  `min_stock` int(11) DEFAULT 1,
  `tag` text NOT NULL,
  `category_id` int(10) UNSIGNED NOT NULL,
  `discount_category_id` int(10) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `rate_product`
--

CREATE TABLE `rate_product` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `user_rate` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `rate_product`
--

INSERT INTO `rate_product` (`id`, `user_id`, `product_id`, `user_rate`, `created_at`, `updated_at`) VALUES
(1, 1, 593, 3, '2021-04-02 00:07:45', '2021-04-02 00:07:45'),
(2, 1, 597, 5, '2021-04-02 00:08:17', '2021-04-02 00:08:17'),
(3, 1, 596, 4, '2021-04-02 00:11:37', '2021-04-02 00:11:37'),
(4, 1, 596, 5, '2021-04-02 00:11:52', '2021-04-02 00:11:52'),
(5, 1, 596, 4, '2021-04-02 01:16:26', '2021-04-02 01:16:26'),
(6, 1, 593, 4, '2021-04-02 01:22:35', '2021-04-02 01:22:35'),
(7, 1, 596, 5, '2021-04-02 01:22:54', '2021-04-02 01:22:54'),
(8, 1, 596, 1, '2021-04-02 01:23:21', '2021-04-02 01:23:21');

-- --------------------------------------------------------

--
-- Table structure for table `sales`
--

CREATE TABLE `sales` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `product_id` text NOT NULL,
  `product_id_only` int(100) DEFAULT NULL,
  `product_quantity` int(100) DEFAULT NULL,
  `order_status` text NOT NULL,
  `price` bigint(20) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sales`
--

INSERT INTO `sales` (`id`, `user_id`, `product_id`, `product_id_only`, `product_quantity`, `order_status`, `price`, `created_at`, `updated_at`) VALUES
(1, 1, '1:3:#000000:1', NULL, NULL, 'Cancel', 285, '2021-03-13 01:04:16', '2021-03-13 01:07:52'),
(2, 1, '1:1:#000000:1', NULL, NULL, 'Placed', 95, '2021-03-14 08:30:08', '2021-03-14 08:30:08'),
(3, 1, '1:1:#000000:1,596:1:#000000:2', NULL, NULL, 'Placed', 1595, '2021-03-21 11:55:54', '2021-03-21 11:55:54'),
(4, 1, '597:2:#000000:1', NULL, NULL, 'Placed', 9084, '2021-03-22 08:45:36', '2021-03-22 08:45:36'),
(5, 1, '596:2:#000000:1,597:3:#000000:2', NULL, NULL, 'Placed', 16626, '2021-03-29 07:24:27', '2021-03-29 07:24:27'),
(6, 1, '596:1:#000000:1', NULL, NULL, 'Placed', 1500, '2021-03-29 07:46:32', '2021-03-29 07:46:32'),
(7, 1, '596:1:#000000:1,597:1:#000000:2,596:1:#000000:3', NULL, NULL, 'Placed', 1500, '2021-03-29 11:56:44', '2021-03-29 11:56:44'),
(8, 1, '596:1:#000000:1,597:1:#000000:2', NULL, NULL, 'Cancel', 6042, '2021-03-30 09:58:03', '2021-03-30 10:01:44'),
(9, 1, '596:1:#000000:1', NULL, NULL, 'Placed', 1500, '2021-03-30 11:17:39', '2021-03-30 11:17:39'),
(10, 1, '598:1:#000000:1', NULL, NULL, 'Placed', 1000, '2021-03-30 11:23:17', '2021-03-30 11:23:17'),
(11, 1, '598:1:#000000:1', NULL, NULL, 'Placed', 1000, '2021-03-30 11:25:20', '2021-03-30 11:25:20'),
(12, 1, '596:1:#000000:1', NULL, NULL, 'Placed', 1500, '2021-03-30 11:35:10', '2021-03-30 11:35:10'),
(13, 1, '596:1:#000000:1', NULL, NULL, 'Placed', 1500, '2021-03-30 11:36:59', '2021-03-30 11:36:59'),
(14, 1, '598:1:#000000:1', NULL, NULL, 'Delivered', 1000, '2021-03-30 11:39:32', '2021-03-30 11:52:08'),
(15, 2, '596:2:#000000:1,598:1:#000000:2', NULL, NULL, 'Placed', 4000, '2021-04-01 21:02:00', '2021-04-01 21:02:00'),
(16, 1, '596:2:#000000:1', NULL, NULL, 'Placed', 3000, '2021-04-01 21:23:11', '2021-04-01 21:23:11'),
(17, 1, '596:2:#000000:1', NULL, NULL, 'Placed', 3000, '2021-04-01 21:23:48', '2021-04-01 21:23:48'),
(18, 1, '596:2:#000000:1', NULL, NULL, 'Placed', 3000, '2021-04-01 21:39:09', '2021-04-01 21:39:09'),
(19, 1, '597:1:#000000:1,596:1:#000000:2', NULL, NULL, 'Placed', 2500, '2021-04-01 21:45:52', '2021-04-01 21:45:52'),
(20, 1, '596:1:#000000:1', NULL, NULL, 'Placed', 1500, '2021-04-01 21:48:15', '2021-04-01 21:48:15'),
(21, 1, '596:1:#000000:1', NULL, NULL, 'Placed', 1500, '2021-04-01 21:53:46', '2021-04-01 21:53:46'),
(22, 1, '596:1:#000000:1', NULL, NULL, 'Placed', 1500, '2021-04-01 21:54:36', '2021-04-01 21:54:36');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `full_name` varchar(255) NOT NULL,
  `email` text NOT NULL,
  `password` text NOT NULL,
  `phone` text NOT NULL,
  `prev_password` text DEFAULT NULL,
  `address_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `full_name`, `email`, `password`, `phone`, `prev_password`, `address_id`, `created_at`, `updated_at`) VALUES
(1, 'amira', 'amirahamdy68@gmail.com', '123456789', '0154454', NULL, 1, '2021-03-13 01:02:08', '2021-03-13 01:02:08'),
(2, 'amira', '', '123456789', '0154454', NULL, 2, '2021-04-01 20:32:12', '2021-04-01 20:32:12');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `addresses`
--
ALTER TABLE `addresses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cart`
--
ALTER TABLE `cart`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sales_user_id_foreign` (`user_id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `categories_name_unique` (`name`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `serial_id` (`serial_id`) USING HASH,
  ADD KEY `category_id` (`category_id`);

--
-- Indexes for table `products_old`
--
ALTER TABLE `products_old`
  ADD PRIMARY KEY (`id`),
  ADD KEY `products_category_id_foreign` (`category_id`),
  ADD KEY `discount_category_id` (`discount_category_id`);

--
-- Indexes for table `rate_product`
--
ALTER TABLE `rate_product`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `product_id` (`product_id`);

--
-- Indexes for table `sales`
--
ALTER TABLE `sales`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sales_user_id_foreign` (`user_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`) USING HASH,
  ADD KEY `users_address_id_foreign` (`address_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `addresses`
--
ALTER TABLE `addresses`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `cart`
--
ALTER TABLE `cart`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=599;

--
-- AUTO_INCREMENT for table `products_old`
--
ALTER TABLE `products_old`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `rate_product`
--
ALTER TABLE `rate_product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `sales`
--
ALTER TABLE `sales`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `cart`
--
ALTER TABLE `cart`
  ADD CONSTRAINT `sales_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `products_old`
--
ALTER TABLE `products_old`
  ADD CONSTRAINT `products_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_address_id_foreign` FOREIGN KEY (`address_id`) REFERENCES `addresses` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
