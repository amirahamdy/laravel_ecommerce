@extends('store.storeLayout_2') 
@section('content')
    <div class="section">
        <!-- container -->
        <div class="container">
            <!-- row -->
            <div class="row">

                <!-- section title -->
                <div class="col-md-12">
                    <div class="section-title">
                        <h3 class="title">Terms & Conditions</h3>

                    </div>
                </div>
                <!-- /section title -->

                <!-- Products tab & slick -->
                <div class="col-md-12">
                    <div class="row">
                    <p style="float:right;">فيما يلى الشروط والبنود التى تتخص استخدامك ودخولك لصفحات الموقع وكافه الروابط والادوات المتفرعه منها Vera  اهلا بكم فى  </p>
               <br><br>
                <p style="float:right;">  ان استخدامك للموقع هو موافقه منك على القبول ببنود وشروط هذه الاتفاقيه والذى يتضمن كافه التفاصيل ادناه وهو تاكيد هذى الالتزامات بالاستجابه لمضمون هذه الاتفاقيه</p>
                    
                    
                    </div>
                    
                </div>
                <!-- /row -->
                <ol>
                        <li style="float:right;">.ت1-تمنح عضوية الموقع فقط لمن تجازوت أعمارهم 18 عام . وللموقع الحق بإلغاءحساب أي عضو لم يبلغ الـ 18 عام مع الإلتزام بتصفية حساباته المالية فور إغلاق الحساب </li>
                        <br><br>
                        <li style="float:right;">  لا يحق لأي شخص إستخدام الموقع إذا ألغيت عضويته-</li>
                        <br><br>
                        <li style="float:right;">-في حال قيام أي مستخدم بالتسجيل كمؤسسة تجارية، فإن مؤسسته التجارية تكون ملزمة بكافة والشروط الواردة في هذه الإتفاقية. </li>
                        <br><br>
                        <li style="float:right;">ينبغي عليك الإلتزام بكافة القوانين المعمول بها لتنظيم التجارة عبر الانترنت-</li>
                        <br><br>
                        <li style="float:right;">لا يحق لأي عضو أو مؤسسة أن تقوم بفتح حسابين بآن واحد لأي سبب كان، ولإدارة الموقع الحق بتجميد الحسابين أو إلغاء أحدهما مع الإلتزام بتصفية كافة العمليات المالية المتعلقة بالحساب قبل إغلاقه- </li>
                       <br><br>
                        <li style="float:right;">على المستخدمي أفراد و مؤسسات الإلتزام بالعقود التجارية المبرمة مع الأعضاء</li>
                        <br><br><br><br>
                        <br><br>
                        <li style="float:right;"></li>

                        <li style="float:right;"></li>
                        <li style="float:right;"></li>
                        <li style="float:right;"></li>
                        <li style="float:right;"></li>
                        <li style="float:right;"></li>
                        <li style="float:right;"></li>
                        <li style="float:right;"></li>

                    </ol>
            </div>
            <!-- /container -->
        </div>
    </div>
@endsection