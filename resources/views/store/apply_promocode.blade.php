@extends('store.storeLayout')
@section('content')
<!-- SECTION -->
<div class="section">
    <!-- container -->
    <div class="container">
        <!-- row -->
        <div class="row">
            <div class="col-md-12">
               

                <form method="post" id="" action="/ApplyPromoCodeSave">
                {{csrf_field()}}
                    <div class="col-md-6" style="float: none;">
                        <!-- Billing Details -->
                        <div class="billing-details">
                            <div class="section-title">
                                <h3 class="title">Add Promo Code</h3>
                            </div>
                            {{Session::get('price')}} EGP
                            <div class="form-group">
                                <input class="input" type="text" name="user_promocode" id="user_promocode" placeholder="Enter your Code" value="" required/>
                            </div>
                            
                                <input type="submit"  name="add_user_promocode" class="primary-btn order-submit" value="Add Promocode"/>
                        </div>
                    </div>
                </form>


            </div>
        </div>
       
    </div>

</div>

@endsection
