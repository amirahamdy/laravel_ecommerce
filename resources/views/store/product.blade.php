@extends('store.storeLayout')
@section('content')
<script src="{{asset('js/lib/jquery.js')}}"></script>
<script src="{{asset('js/dist/jquery.validate.js')}}"></script>
 <script data-require="jquery@3.1.1" data-semver="3.1.1" src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

<link type="text/css" rel="stylesheet" href="{{asset('css/style_for_quantity.css')}}" />

<style>
label.error {
  color: #a94442;
  background-color: #f2dede;
  border-color: #ebccd1;
  padding:1px 20px 1px 20px;
}


</style>

<!-- SECTION -->
<div class="section">
    <!-- container -->
    <div class="container">
        <!-- row -->
        <div class="row">
            <!-- Product main img -->
            <div class="col-md-5 ">
                <div id="#">
                    <div class="product-preview">
                         <!--
                        <img src="../uploads/products/{{$product->id}}/{{$product->image_name}}" alt="">
                       
                        <img src="../uploads/products/El Waleed web photos/{{$product->image_name}}" alt="">
                        -->

                         <tr>
                            @foreach ($image_name as $picture) 
                                <td>  
                                <img class="mySlides" src="../uploads/products/{{$product->serial_id}}/{{$picture}}" />
                                </td>
                            @endforeach

                            <button class="w3-button w3-display-left" onclick="plusDivs(-1)">&#10094;</button>
                            <button class="w3-button w3-display-right" onclick="plusDivs(+1)">&#10095;</button>
                        </tr>

                    </div>
                </div>
            </div>
            <!-- /Product main img -->


            <!-- Product details -->
            <div class="col-md-5">
                <div class="product-details">
                    <h2 class="product-name">{{$product->name}}</h2>
                    
                        <div class="product-rating">
                        {{$rate}}
                    @if($rate != null)       
                        @if($rate == 5)
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                        @elseif($rate == 4)
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star-o"></i>
                        @elseif($rate == 3)
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star-o"></i>
                                    <i class="fa fa-star-o"></i>
                        @elseif($rate == 2)
                        
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star-o"></i>
                                    <i class="fa fa-star-o"></i>
                                    <i class="fa fa-star-o"></i>

                        @elseif($rate <= 1)
                       
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star-o"></i>
                                    <i class="fa fa-star-o"></i>
                                    <i class="fa fa-star-o"></i>
                                    <i class="fa fa-star-o"></i>

                        @endif     
                    @endif
                     
                    </div>
                    <div>
                        <h3 class="product-price">EGP {{$product->discount}} <del class="product-old-price">EGP {{$product->price}}</del></h3>
                        </br>
                        <h3 class="product-price">Stock: {{$product->stock}} </h3>

                        @if($product->stock > $product->min_stock)
                             <span class="product-available">In Stock</span>
                        @else
                             <span class="product-available">OUt Of Stock</span>
                        @endif
                    </div>
                    <p>{!!$product->description!!}</p>
                    <p>{!!$product->description_arabic!!}</p>
                    <form method="post" id="order_form">
                    {{csrf_field()}}
                    <div class="product-options" >
                        <input type="hidden" id="discount_price_holder" name="discount_price_holder" value={{$product->discount}}>
                        <label>
                        
                        <div id="field1">Quantity
                        
                        <input type="number" id="quantity" name="quantity" value="1" min="1" max={{$product->stock}}  />
                        
                         </div>
                        
                        </label>
                       
                        <input style="display: none;" type="radio" name="color"  value="{{isset($colors) ? $colors : ''}}" checked >
                        <!--
                        <div style="display: none;height:25px;width:25px;margin:5px;display:inline-block;background-color: {{$colors[1]}}"></div>
                       -->
                          
                    </div>
                        <div id="for_error"></div>

                    <div class="add-to-cart">
                        <button type="submit" name="myButton" id="myButton" class="add-to-cart-btn"><i class="fa fa-shopping-cart"></i> add to cart</button>
                    </div>
                    </form>
                    <ul class="product-links">
                        <li>Category:</li>
                        <li><a href="{{route('user.search')}}?c={{$product->category->id}}">{{$product->category->name}}</a></li>
                    </ul>
                </div>
            </div>
            <!-- /Product details -->

        </div>
        <!-- /row -->
    </div>
    <!-- /container -->
</div>
<div style="height:200px"></div>

<!--JQUERY Validation-->
<script>
	
    /*
    $(document).ready(function() {
		
		$("#order_form").validate({
			
            submitHandler: function (form) {
            if($('input[name=color]:checked').val()==undefined)
            {
                
            document.getElementById("for_error").innerHTML = "<label class='error' style=' '>Please,Choose Color!</label>";

            }
                else
                    {
                        return true;
                    }
                
         }
		});

		
	});
	
    $('.add').click(function () {
        
        $(this).prev().val(+$(this).prev().val() + 1);
        
    });
    $('.sub').click(function () {
            if ($(this).next().val() > 1) {
            $(this).next().val(+$(this).next().val() - 1);
            }
    });
    
	*/
   
	</script>


    <script>
var slideIndex = 1;
showDivs(slideIndex);

function plusDivs(n) {
  showDivs(slideIndex += n);
}

function showDivs(n) {
  var i;
  var x = document.getElementsByClassName("mySlides");
  if (n > x.length) {slideIndex = 1}
  if (n < 1) {slideIndex = x.length} ;
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  x[slideIndex-1].style.display = "block";
}
    </script>
<!--/JQUERY Validation-->
<!-- /SECTION -->
@endsection
