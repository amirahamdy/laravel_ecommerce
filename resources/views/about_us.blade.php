@extends('store.storeLayout_2') 
@section('content')
    <div class="section">
        <!-- container -->
        <div class="container">
            <!-- row -->
            <div class="row">

                <!-- section title -->
                <div class="col-md-12">
                    <div class="section-title">
                        <h3 class="title">About us</h3>

                    </div>
                </div>
                <!-- /section title -->

                <!-- Products tab & slick -->
                <div class="col-md-12">
                    <div class="row">
                    <p style="float:right;"> من المؤسسات المتواجده التى تعمل فى قطع غيار الالكترونيات منذ عام 1990 Vera مؤسسه الوليد للالكترونيات وعلامتها التجاريه </p>
                    <p style="float:right;">يضم اكثر من 10.000 منتج خاص بالالكترونيات من قطع غيار واكسسوارات وجميع ما يحتاجه البيت العصرى من الكترونيات</p>
                    <br><br><br>
                    <p style="float:right;">تأسست عام 1990 ورائد فى مجال لالكترونياتوالتكنولوجيا الحديثه- تقوم مؤسسهاالوليد بأستيرادقطع غيار الالكترونيات خصيصا فى اجود المصانع بالصين وماليزيا وفيتنام واليابان  بحيث تتضمن جميع المنتجات </p>
                    <p style="float:right;">التى تشمل العلامه التجاريه ضمان شامل من حيث الجوده والعمر الافتراضي وضدد عيوب الصناعه</p>
                    <p style="float:right;">المقر الرئيسي : شارع ملك حفنى - اتحاد ملاك برج العهد الجديد - العصافره محل رقم 2 تقاطع الملازم بسيونى مع الملك حفنى بجوار مستشفى مبرة العصافره - الاسكندريه</p>
   
                    </div>
                    
                </div>
                <!-- /row -->
            </div>
            <!-- /container -->
        </div>
    </div>
@endsection