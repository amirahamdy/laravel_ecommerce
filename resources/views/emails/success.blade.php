@section('content')
<script src="{{asset('js/lib/jquery.js')}}"></script>
<script src="{{asset('js/dist/jquery.validate.js')}}"></script>

<style>
label.error {
  color: #a94442;
  background-color: #f2dede;
  border-color: #ebccd1;
  padding:1px 20px 1px 20px;
}</style>
<!-- SECTION -->
<div class="section">
    <!-- container -->
    <div class="container">
        <!-- row -->
        <div class="row">
        
        <p class="" data-lead-id="main-content-body">Thank you for your order no. ,Please check your email for more details</p>


        </div>
        <!-- /row -->
    </div>
    <!-- /container -->
</div>

<!-- /SECTION -->
@endsection
