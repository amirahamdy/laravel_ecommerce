<html>
<head>
</head>
<body>

    <table class="table table-striped" width="100%">
        <thead>
          <tr>
        <th>User Name</th>
        <th>Email</th>
        <th>Phone</th>
        <th>Arabic Description</th>
        <th>Address City</th>
        <th>Address ZIP</th>
        <th>Address  Area</th>
        
        
        </tr>
        </thead>

        <tbody>
          @if(isset($customers))
          @foreach($customers as $customer)
           
            <tr>
                <td>  {{isset($customer->full_name) ? $customer->full_name : ''}} </td>

                <td>  {{isset($customer->email) ? $customer->email : ''}} </td>

                <td>  {{isset($customer->addresses->city) ? $customer->addresses->city : ''}} </td>


                <td>  {{isset($customer->addresses->zip) ? $customer->addresses->zip : ''}} </td>

                <td>  {{isset($customer->addresses->area) ? $customer->addresses->area : ''}} </td>


                {{ csrf_field() }}


            </tr>
            
            @endforeach
            @endif
        </tbody>
    </table>

</body>
</html>