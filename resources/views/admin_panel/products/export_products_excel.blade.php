<html>
<head>
</head>
<body>

    <table class="table table-striped" width="100%">
        <thead>
          <tr>
        <th>Serial ID</th>
        <th>Product Name</th>
        <th>Description</th>
        <th>Arabic Description</th>
        <th>Price</th>
        <th>Discount</th>
        <th>Original Stock</th>
        <th>Tag</th>
        <th>Rate</th>
        <th>Created At</th>
        
        </tr>
        </thead>

        <tbody>
          @if(isset($products))
          @foreach($products as $product)
           
            <tr>
                <td>  {{isset($product->serial_id) ? $product->serial_id : ''}} </td>

                <td>  {{isset($product->name) ? $product->name : ''}} </td>

                <td>  {{isset($product->description) ? $product->description : ''}} </td>


                <td>  {{isset($product->description_arabic) ? $product->description_arabic : ''}} </td>

                <td>  {{isset($product->price) ? $product->price : ''}} </td>

                <td>  {{isset($product->discount) ? $product->discount : ''}} </td>

                <td>  {{isset($product->stock) ? $product->stock : ''}} </td>

                <td>  {{isset($product->tag) ? $product->tag : ''}} </td>
                <td>  {{isset($product->rate) ? $product->rate : ''}} </td>
                <td>  {{isset($product->created_at) ? $product->created_at : ''}} </td>



                {{ csrf_field() }}


            </tr>
            
            @endforeach
            @endif
        </tbody>
    </table>

</body>
</html>