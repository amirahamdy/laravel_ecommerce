@extends('admin_panel.adminLayout') @section('content')
<script src="{{asset('js/lib/jquery.js')}}"></script>
<script src="{{asset('js/dist/jquery.validate.js')}}"></script>
<style>label.error {
  color: #a94442;
  background-color: #f2dede;
  border-color: #ebccd1;
  padding:1px 20px 1px 20px;
}</style>



<div class="content-wrapper">
    <div class="row">
        <div class="col-12 stretch-card">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Add PromoCode</h4>
                    <form class="forms-sample" method="post" id="cat_form">
                        {{csrf_field()}}
                        <div class="form-group row">
                            <label for="exampleInputEmail2" class="col-sm-3 col-form-label">Code</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" name="code" id="code" placeholder="Enter Code">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="exampleInputEmail2" class="col-sm-3 col-form-label">Value</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" name="value" id="value" placeholder="Enter Discount Value">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="exampleInputPassword2" class="col-sm-3 col-form-label">Finished At</label>
                            <div class="col-sm-9">
                                <input type="date" class="form-control" name="finished_at" id="finished_at" placeholder="Enter Finished At">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="exampleInputEmail2" class="col-sm-3 col-form-label">Description</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" name="description" id="description" placeholder="Enter Description">
                            </div>
                        </div>
                      
                        <button type="submit" class="btn btn-success mr-2">Submit</button>
                    </form>
                    @if($errors->any())
                    <ul>
                        @foreach($errors->all() as $err)
                        <tr>
                            <td>
                                <li>{{$err}}</li>
                            </td>
                        </tr>
                        @endforeach
                    </ul>
                    @endif
                </div>
            </div>
        </div>

        <div class="col-lg-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">PromoCodes Table</h4>
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>
                                    Code
                                    </th>
                                    <th>
                                        Value
                                    </th>
                                    <th>
                                    Finished At
                                    </th>
                                    <th>
                                    Description
                                    </th>
                                    <th>
                                        Created At
                                    </th>
                                    <th>
                                        Updated At
                                    </th>
                                    <th>
                                        Edit
                                    </th>
                                    <th>
                                        Update
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($promolist as $cat)
                                <tr>
                                    <td>
                                        {{$cat->code}}
                                    </td>
                                    <td>
                                        {{$cat->value}}
                                    </td>
                                    <td>
                                    {{$cat->finished_at}}
                                    </td>
                                    <td>
                                    {{$cat->description}}
                                    </td>
                                    <td>
                                        {{$cat->created_at}}
                                    </td>
                                    <td>
                                        {{$cat->updated_at}}
                                    </td>
                                    <td>
                                        <a href="{{route('admin.PromoCodes.edit', ['id' => $cat->id])}}" class="btn btn-warning">Edit</a>
                                    </td>
                                    <td>
                                        <a href="{{route('admin.PromoCodes.delete', ['id' => $cat->id])}}" onclick="delete()" class="btn btn-danger">Delete</a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<!--JQUERY Validation-->
<script>
	
	$(document).ready(function() {
		
		$("#cat_form").validate({
			rules: {
				code: "required",
				value: "required",
                finished_at: "required",
				
				
				
			},
			messages: {
				code: "Code is Required",
				value: "Value is Required",
                finished_at: "Finished At is Required",
                	
			}
		});

		
	});
	</script>
<!--/JQUERY Validation-->

@endsection
