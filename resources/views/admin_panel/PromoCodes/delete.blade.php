@extends('admin_panel.adminLayout') @section('content')
<div class="content-wrapper">
          <div class="row">
            <div class="col-md-12 d-flex align-items-stretch grid-margin">
              <div class="row flex-grow">
                <div class="col-12">
                  <div class="card">
                    <div class="card-body">
                    <a href="{{route('admin.PromoCodes')}}"> < Back to List</a>
                    <br><br>
                      <h4 class="card-title">Delete Promo Code</h4>
                      <br>
                      <form class="forms-sample" method="post">
                      {{csrf_field()}}
        
                        <div class="form-group">
                          <label for="exampleInputEmail1">Code</label>
                          <input type="text" class="form-control" id="code" name="code" value="{{$category->code}}"disabled>
                        </div>
                        <div class="form-group">
                          <label for="exampleInputEmail1">value</label>
                          <input type="text" class="form-control" id="value" name="value" value="{{$category->value}}"disabled>
                        </div>
                        <div class="form-group">
                          <label for="exampleInputEmail1">Finished At</label>
                          <input type="date" class="form-control" id="finished_at" name="finished_at" value="{{$category->finished_at}}"disabled>
                        </div>
                        <div class="form-group">
                          <label for="exampleInputPassword1">Description</label>
                          <textarea type="textarea" class="form-control" id="description" name="description"disabled >{{$category->description}}</textarea>
                        </div>
                        <input  type="submit" name="updateButton"  class="btn btn-danger mr-2" id="updateButton" value="DELETE" />
                      </form>
                    </div>
                  </div>
                </div>

              </div>
            </div>
          </div>
        </div>
@endsection