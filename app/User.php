<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'full_name',
        'email',
        'password',
        'phone',
        'prev_password',
        'address_id'
    ];

    public function addresses()
    {
    	return $this->hasOne('App\Address', 'id', 'address_id');
    }

    public function user_cart(){
        return $this->hasMany(Cart::Class,'user_id','id');
      }

      public function user_sales(){
        return $this->hasMany('App\sale','user_id','id');
      }  
}
