<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rate extends Model
{
    //


    protected $table = "rate_product";

    public function user(){
      return $this->belongsTo(User::Class,'user_id','id');
    }
}
