<?php

namespace App\Http\Controllers\admin_panel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\ProductVerifyRequest;
use App\Http\Requests\ProductEditVerifyRequest;


use Illuminate\Support\Facades\DB;
use App\Product;
use App\Category;

use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Facades\Storage;

use File;
use Excel;


class productsController extends Controller
{
   public function index()
    {
        $result = Product::paginate(10);

    	return view('admin_panel.products.index')
    		->with('prdlist', $result);
        
    }
    
     public function create()
    {
        $result = Category::all();
        return view('admin_panel.products.create')
            ->with('catlist', $result);
        
    }
    
    
    
    public function store(ProductVerifyRequest $request)
    { 
        //dd($request->images);
      
        
        $this->validate($request,[
            'serial_id' =>'required|unique:products',
            'main_image' =>'required',
            'images' =>'required',
            //'images.*'=>'image|mimes:jpeg,png',

        ]);

        ///////////////////// code for gallery /////////////////////////
            $data=array();
            if($request->hasfile('images')){
                foreach($request->file('images') as $image){

                    $name = $image->getClientOriginalName();
                    $image ->move(public_path().'/uploads/products/'.$request->serial_id,$name);
                    
                    $data[]=$name;

                  
                }

            }
      
            /////////////////// code for main image /////////////////////////////

            if($request->hasfile('main_image')){
                //dd($request->main_image);
                $pic = $request->file('main_image');
                //dd($pic->getClientOriginalName());
                $name = $pic->getClientOriginalName();
               // dd($name);
                $pic->move(public_path().'/uploads/products/',$name);
            }

        $prd = new Product();
        $prd->serial_id = $request->serial_id;
        $prd->main_image = $name;
        $prd->image_name =  json_encode($data);
        $prd->name = $request->Name;
        $prd->description = $request->Description;
        $prd->description_arabic = $request->Description_arabic;
        $prd->category_id = $request->Category;

        //$prd->discount_category_id = $request->Category;

        $prd->price = $request->Price;
        $prd->discount = $request->Discounted_Price;
        
        $prd->original_stock = $request->Stock;
        $prd->stock = $request->Stock;
        $prd->min_stock = $request->Min_Stock;

        $prd->colors = $request->Colors;
        $prd->tag = $request->Tags;
        $prd->save();
        //dd($prd);
      

            //return back()->with('success','Your images has been moved');
        return redirect()->route('admin.products');
  
        
    }
    
    
    public function edit($id)
    {
        $cat = Category::all();
 
        $prd = Product::find($id);    
        
    
        return view('admin_panel.products.edit')
            ->with('product', $prd)
            ->with('catlist', $cat)
            ->with('select_attribute', '');

            
    }

    public function update(ProductEditVerifyRequest $request, $id)
    {
              ///////////////////// code for gallery /////////////////////////
        
        $data=array();
        if($request->hasfile('images')){
            foreach($request->file('images') as $image){

                $name = $image->getClientOriginalName();
                $image ->move(public_path().'/uploads/products/'.$request->serial_id,$name);
                
                $data[]=$name;

              
            }

        }
        
        /////////////////// code for main image /////////////////////////////

        if($request->hasfile('main_image')){
        $main_image = $request->file('main_image');
        $name = $main_image->getClientOriginalName();
        
        $main_image ->move(public_path().'/uploads/products/',$name);
        }
        
        $prdToUpdate = Product::find($request->id);
        
        $prdToUpdate->serial_id = $request->serial_id;
        //$prdToUpdate->main_image = $name;
        //$prdToUpdate->image_name =  json_encode($data);
        $prdToUpdate->name = $request->Name;
        $prdToUpdate->description = $request->Description;
        $prdToUpdate->description_arabic = $request->Description_arabic;
       
        $prdToUpdate->price = $request->Price;
        $prdToUpdate->discount= $request->Discounted_Price;

        $prdToUpdate->original_stock = $request->Stock;
        $prdToUpdate->stock = $request->Stock;
        $prdToUpdate->min_stock = $request->Min_Stock;

        $prdToUpdate->category_id = $request->Category;
        //$prdToUpdate->discount_category_id = $request->Category;

  
        $prdToUpdate->colors = $request->Colors;
        $prdToUpdate->tag= $request->Tags;
        
     
        $prdToUpdate->save();
        

            
        return redirect()->route('admin.products');        
        
    }
    
    public function delete($id)
    {
       
        $prd = Product::find($id);

        return view('admin_panel.products.delete')
            ->with('product', $prd);
    }

    public function destroy(Request $request)
    {
        
       
        $prdToDelete = Product::find($request->id);
        $delete = Product::where('id','=',$prdToDelete->id)->delete();

        /*
        $folderPath = public_path().'/uploads/products/'.$request->serial_id;

        //system('rm -rf -- ' . escapeshellarg($dir), $retval);

        //$dd = rmdir($folderPath);

        $dd = File::deleteDirectory($folderPath);

        */


        $folderPath = public_path().'/uploads/products/'.$prdToDelete->serial_id.'/';
        //dd($folderPath);
        $response = File::deleteDirectory($folderPath);
     
        //dd($response);
        
        return redirect()->route('admin.products');
        
    }//end func


    public function export_products_excel(){

        $products = Product::get();
        //dd($products);
        Excel::create('Products', function($excel) use ($products) {
                  $excel->sheet('Products', function($sheet) use ($products)
                  {
                     $sheet->setStyle(array(
                      'font' => array( 'family' => 'Calibri',
                             'size' => '13',
                             'bold' => true)
  
                     ));
  
                  $sheet->setBorder('A1:AM1', 'thick');
                  $sheet->row(1, function($row) { $row->setBackground('#CCCCCC'); });
  
  
                  //$sheet->fromArray($data);
                  $sheet->loadView('admin_panel.products.export_products_excel')->with('products',$products);
                  });
              })->download('xlsx');
  
      } // end of export_excel
    
   
    
    
}//end class 
