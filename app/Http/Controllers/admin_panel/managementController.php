<?php

namespace App\Http\Controllers\admin_panel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Product;
use App\Category;
use App\sale;
use App\User;
use App\Address;
use Excel;

class managementController extends Controller
{
    public function manage()
    {
    	$res1= sale::all();
        if(!$res1)
        {
			return view('admin_panel.dashboard.orderManagement')->with('all',[])
	         ->with('products',[])
	         ->with('sale',[]);
        }
        
        $cart=[];
        $product=[];
        $users=[];
        foreach($res1 as $r )
        {

           // echo "select * from users inner join addresses on users.address_id = addresses.id where users.id = $r->user_id" .'<br>';
            $users[] = DB::select( DB::raw("select users.id as id , users.full_name as full_name , addresses.area as area , addresses.city as city , addresses.zip as zip from users inner join addresses on users.address_id = addresses.id where users.id = $r->user_id" ) )[0];
             //$users[]=User::find($r->user_id)->with('addresses')->get();
             $totalCart = explode(',',$r->product_id);
             foreach($totalCart as $c)
             {
                $cart[]=array_prepend(explode(':',$c), $r->id);
                $a=explode(':',$c);
                $res = Product::find($a[0]);
                $product[]=$res;
             }
        }
        //dd($users);
        //dd($users[0]->area);

        /* sent to view ordes index.php  */
        //dd($cart);
         return view('admin_panel.orders.index')->with('all',$cart)
         ->with('products',$product)
         ->with('sale',$res1)
         ->with('users',$users)
         //->with('status',['Placed','On Process','Delivered','Cancel']);
         ->with('status',['Placed','Delivered','Cancel']);

    }
    //
    public function update(Request $r)
    {
    	$n=sale::find($r->orderId);
        //dd($n->product_id);  // this is cart 
        $cart=[];
        $product=[];

    	if($n)
    	{
            //dd($r->stat);
    		$n->order_status=$r->stat;
    		$n->save();

            if($r->stat == 'Cancel'){

                //$c = $n->product_id;   // this is cart 
                $totalCart = explode(',',$n->product_id);
                foreach($totalCart as $c){
                  
                    $cart[]=explode(':',$c);
                    $a=explode(':',$c);
                    $res = Product::find($a[0]);
                    $product[]=$res;
                    //// VIP  update stock of product in DB if Cancel Done////////
                    if($res->stock < $res->original_stock){

                        $res->stock= $res->stock + $a[1];
                        $res->save();
                    }
                }       
            }
    	}

    	$res1= sale::all();
        if(!$res1)
        {
			return view('admin_panel.dashboard.orderManagement')->with('all',[])
	         ->with('products',[])
	         ->with('sale',[]);
        }
       
        $cart=[];
        $product=[];
        foreach($res1 as $r )
        {
             $totalCart = explode(',',$r->product_id);
             foreach($totalCart as $c)
             {
                $cart[]=array_prepend(explode(':',$c), $r->id);
                $a=explode(':',$c);
                $res = Product::find($a[0]);
                $product[]=$res;
             
             }
        }
     
         
        return redirect()->route('admin.orderManagement');

    }//end fn 

    public function export_customers_excel(){

        $customers = User::get();
        //dd($products);
        Excel::create('Customers', function($excel) use ($customers) {
                  $excel->sheet('Customers', function($sheet) use ($customers)
                  {
                     $sheet->setStyle(array(
                      'font' => array( 'family' => 'Calibri',
                             'size' => '13',
                             'bold' => true)
  
                     ));
  
                  $sheet->setBorder('A1:AM1', 'thick');
                  $sheet->row(1, function($row) { $row->setBackground('#CCCCCC'); });
  
  
                  //$sheet->fromArray($data);
                  $sheet->loadView('admin_panel.orders.export_customers_excel')->with('customers',$customers);
                  });
              })->download('xlsx');
  
      } // end of export_excel
}//end class
