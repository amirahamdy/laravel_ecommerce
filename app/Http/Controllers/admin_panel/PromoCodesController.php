<?php

namespace App\Http\Controllers\admin_panel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\PromoCode;

class PromoCodesController extends Controller
{
    //
    public function index()
    {
        $result = PromoCode::all();

    	return view('admin_panel.PromoCodes.index')
    		->with('promolist', $result);
        
    }

    public function posted( Request $request)
    {

        $PromoCode = new PromoCode();

        $PromoCode->code = $request->code;
        $PromoCode->value = $request->value;
        $PromoCode->finished_at = $request->finished_at;
        $PromoCode->description = $request->description;

        $PromoCode->save();

        return redirect()->route('admin.PromoCodes');
    }

    public function edit($id)
    {
        

        $cat = PromoCode::find($id);
        
        return view('admin_panel.PromoCodes.edit')
            ->with('category', $cat);
    }

    public function update(Request $request, $id)
    {
      
        $catToUpdate = PromoCode::find($request->id);

        $catToUpdate->code = $request->code;
        $catToUpdate->value = $request->value;
        $catToUpdate->finished_at = $request->finished_at;
        $catToUpdate->description = $request->description;
        $catToUpdate->save();
        
        return redirect()->route('admin.PromoCodes');
    }
    public function delete($id)
    {
       
        $cat = PromoCode::find($id);

        return view('admin_panel.PromoCodes.delete')
            ->with('category', $cat);
    }

    public function destroy(Request $request)
    {   
        

        $catToDelete = PromoCode::find($request->id);
        $catToDelete->delete();

        return redirect()->route('admin.PromoCodes');
    }


}// end class
