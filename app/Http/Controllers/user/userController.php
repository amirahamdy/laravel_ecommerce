<?php
namespace App\Http\Controllers\user;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\orderRequest;
use Illuminate\Support\Facades\DB;

use App\Product;
use App\Category;
use App\sale;
use App\User;
use App\Address;
use Session;
use Mail;
use Auth;
use App\Cart;
use App\Rate;
use App\PromoCode;



class userController extends Controller
{
    public function index()
    {
        $cat = Category::all();
    	$res = Product::all();

        if(Session::has('user')) {
            // must be The user is logged in...
            //dd(Session::get('user')); // Done get id as uniuqe insted of Auth::user()->id
        }
        //dd($res);
        // get all image from db
       /* $images = DB::table('products')
                    ->select('id','image_name')
                    ->get();
         */           
        //dd($images);
        //$image_name = json_decode($res->image_name);
        //dd($image_name);
    	return view('store.index')
                //->with('images',$images)
                ->with('products', $res)
                ->with("cat", $cat)
                ->with('index', 1);
    }
    public function view($id)
    {
        
        $res = Product::find($id);
        $res1 = Product::all();
        $cat=Category::find($res->category_id);
        //$colorList = explode(',',$res->colors);
        $colorList = $res->colors;

    	$cat = Category::all();
        // get image from db
        $image_name = json_decode($res->image_name);
        //dd($image_name);
        /**************** product rate **************/

        $avg_stars = DB::table('rate_product')->where('product_id','=',$id)->avg('user_rate');
        //$rate = Rate::where('product_id','=',$id)->get()->all();
        //$avg_rate_product = $rate->avg('user_rate');
        //dd($rate[0]->user_rate);
        $rate_after_round = round($avg_stars,1);  // to get number with one dicemal places
        //dd(number_format((float)$rate_after_round,0, '.', ''));
        //dd(format_rate); 
        
        return view('store.product')
            ->with('image_name',$image_name)
            ->with('product', $res)
            ->with('products', $res1)
            ->with('cat', $cat)
            ->with('colors',$colorList)
            ->with('rate',number_format((float)$rate_after_round,0, '.', ''));   
    }

    public function search(Request $r){
        $category ;
        $name ;
        if($r->query("c")){
            $category = $r->query("c");
        }
        if($r->query("n")){
            $name = $r->query("n");
        }
        $res = Product::all();
        $cat = Category::all();

        if(isset($category) && isset($name)){
            $name = strtolower($name);
            $sRes = DB::select( DB::raw("SELECT * FROM `products` WHERE lower(name) like '%$name%' and category_id = $category" ) );
            //dd("SELECT * FROM `products` WHERE lower(name) like '%$name%' and category_id = $category" );
            //$a = 0;
        }
        else if(isset($name)){
            $name = strtolower($name);
            $sRes = DB::select( DB::raw("SELECT * FROM `products` WHERE lower(name) like '%$name%'" ) );
          //dd("SELECT * FROM `products` WHERE lower(name) like '%$name%'" );
           // $a = 1;
        }
        else if(isset($category)){
            $sRes = DB::table('products')
            ->where("category_id" , $category)
            ->get();
            //$a = 2;
        }
        else{
            $sRes = DB::table('products')
            ->get();
           // $a= 3;
        }

        if(!isset($category)) {
            $category = -1;
        }
        //dd($sRes);
    	return view('store.search')
            ->with('products', $sRes)
            ->with("cat", $cat)
            ->with("a", $category);
    }

    //********This is function that used in button add to cart user*********** */
     //********This is function that used in button add to cart user*********** */

      public function post($id,orderRequest $r)
      {
          //Session::forget('cart'); // to clear session
         // dd($id);

          if(Session::has('user')) {
               // must be The user is logged in...
              // dd(Session::get('user')); // then get id as uniuqe insted of Auth::user()->id
                
                if(!(Session::has('cart')))
                {
                    Session::put('orderCounter',1);
                    $c=$id.":".$r->quantity.":".$r->color.":".Session::get('orderCounter');   //the order counter is added after color so that order serial can be obtained
                    Session::put('cart',$c);
                }
                else
                {
                    
                    Session::put('orderCounter',Session::get('orderCounter')+1);
                    $cd=$id.":".$r->quantity.":".$r->color.":".Session::get('orderCounter');
                    $total=Session::get('cart').",".$cd;
                    Session::put('cart',$total);
                }
                

        //dd(session()->all());
        //dd(Session::get('user')->id);
        // add record in table cart
                $user_pro_cart = Cart::where('user_id','=',Session::get('user')->id)->get()->first();
                if(!$user_pro_cart){ // to save in DB
                    $user_pro_cart= new cart();
                
                    $user_pro_cart->user_id=session('user')->id;
                    $user_pro_cart->product_id=session('cart');
                    $user_pro_cart->cart_status='0';
                    //$user_pro_cart->price=session('price');
                            
                    $user_pro_cart->save();
                }else{ // update his cart
                    $user_pro_cart->user_id=session('user')->id;
                    $user_pro_cart->product_id=session('cart');
                    $user_pro_cart->cart_status='0';
                    //$user_pro_cart->price=session('price');
                            
                    $user_pro_cart->save();
                }    

            return redirect()->route('user.home');
        }else{

            return redirect('/signup');        

        } 
     
    }//end fn post ... persuched button




    //this is to view user cart route   /cart
    public function cart(Request $r)
    {   
        //Session::forget('cart'); // to clear session
       // dd(session()->all());

        $res = Product::all();
        $cat = Category::all();
        /*if(!Session::has('cart')) //user has no cart
        {
            return view('store.cart')->with('all',null)
            ->with('products',[])
            ->with('products', $res)
            ->with("cat", $cat);
        }*/
        $cart=[];
        $product=[];
        $cost=0;
        $cost_after_quantity=0;
         //dd(Session::get('cart'));
    if(Session::get('user')){

            //get user cart from table cart  , 'cart_status','=',0  this is mean user not confirm order 
            $user_pro_cart = Cart::where('user_id','=',Session::get('user')->id)      //or session('user')->id
                                    ->where('cart_status','=',0)  // not confirmed
                                        ->get()->first();

            if($user_pro_cart != null){
            // done this is as that come from session
            //dd($user_pro_cart->product_id); 
            //$totalCart = explode(',',Session::get('cart'));
            $totalCart = explode(',',$user_pro_cart->product_id);

        
            foreach($totalCart as $c)
            {
                $cart[]=explode(':',$c);
                $a=explode(':',$c);
                $res = Product::find($a[0]);
                $product[]=$res;
                //dd($a);

                //dd($res->min_stock);
                //dd($res->stock);
                //dd($res->id);
            // dd($a[1]);

                $cost_after_quantity = $a[1]*$res->discount;

                //$sub_total += ($item['quantity'] * $product['price']);

                $cost+= $cost_after_quantity;
                Session::put('price',$cost);

            
            }
            
            /*if($a[1] > $res->min_stock){
                    return view('errors');
                }*/
            return view('store.cart')
                ->with('products', $res)
                ->with("cat", $cat)
                ->with('all',$cart)
                ->with('prod',$product)
                ->with('total',Session::get('price'));
            }
            else{
                return redirect('/');
            }
        }//if session 
        else{
            return redirect('/');
        } 
    }// end fn cart

//public function deleteCartItem(Request $r)
public function deleteCartItem ($pro_id)

{
//dd($pro_id);    // done
    
    $counter=0;
    
        $user_Cart = Cart::where('user_id','=',Session::get('user')->id)
                            ->where('cart_status','=',0)  // not confirmed
                            ->get()->first();
        //dd($user_Cart);
        if( ($user_Cart) != null){

            //dd($user_Cart->product_id);
            $newtotalCart = explode(',',$user_Cart->product_id);
            $count = count($newtotalCart);
            //dd(session::get('cart'));
            //dd($count);
            //dd($newtotalCart);
            foreach($newtotalCart as $c)
            {
                //dd($c);
                $newcart[]=explode(':',$c);
                //dd($newcart[0]);
                if($newcart[0] = $pro_id){
                    
                        if($count > 1){
                        //Session()->flush(); // will make sige out 
                        session::forget('cart');
                        unset($newtotalCart[0]);//remove second element, do not re-index array
                        //echo"done";
                        $mytotal=$c;      
                        $user_Cart->product_id = $mytotal;
                        $user_Cart->save();
                        return redirect()->back();
                    
                    }
                    elseif($count == 1){
                        //Session()->flush();
                        session::forget('cart');
                        $user_Cart->delete(); 
                        //echo"hamadaaaaaaaaaaaa";
                        return redirect('/');
    
                    } //if last
                            
                }
               
               
            }//foreach
   
        }//! null
        else{
            return redirect('/');
        }


}//end fn 

    
    
    public function confirm(Request $r)
    {  

        //we need to make fn confirm email
        if($r->has('order'))
        {
            if(Session::has('user'))
            {
                $cart=[];
                $product=[];
                $cost=0;
                $cost_after_quantity=0;
                $user_Cart = Cart::where('user_id','=',Session::get('user')->id)
                                   ->where('cart_status','=',0)  // not confirmed
                                   ->get()->first();
                //dd($user_Cart->product_id);
                $totalCart = explode(',',$user_Cart->product_id);
                //dd(Session::get('cart'));
                
                foreach($totalCart as $c)
                {
                    $cart[]=explode(':',$c);
                    $a=explode(':',$c);
                    //dd($a);
                    $res = Product::find($a[0]);
                    $product[]=$res;
                    ///////////////////// VIP  update stock of product in DB ///////////////////
                   //dd($res);
                    $res->stock= $res->stock - $a[1];               
                    $res->save();
                          
                    //dd($res->min_stock);
                    //dd($res->stock);
                                   
                }

                  ///////////////////// VIP first must update cart status =1 in DB ///////////////////
                  $user_Cart->cart_status= 1;               
                  $user_Cart->save();
                //dd( $r->session());
                //second add record in table sales
                $sales= new sale();

                $sales->user_id=session('user')->id;
                $sales->product_id=session('cart');
                // amira edit session
                //$sales->product_id_only=session('product_id_only');
                //$sales->product_quantity=session('product_quantity');

                $sales->order_status='Placed';
                if(session('new_price')){
                    $sales->price=session('new_price');
                }else{
                    $sales->price=session('price');
                }
                
               
                $sales->save();
                //dd($sales->id);
                
                Session::forget('cart');
                Session::forget('price');
                Session::forget('orderCounter');
                // amira edit session
              
                //dd( $r->session());

                /********************Email  */

                $user = User::findOrFail(session('user')->id);
                //dd($user);
                //dd($app_id->status);
                    Mail::send('emails.confirm_mail', ['user' => $user ,'order_id'=> $sales ], function ($m) use ($user,$sales) {
        
                    $m->from('vera.electronic1990@gmail.com', 'No-reply');
        
                    $m->to($user->email, $user->full_name)->subject(' Vera Confirmation Order');
                });
        
                if(Mail::failures()){
                    return view('emails.email_failed');
                }
                else{
                    
                    //return redirect()->route('emails.success');
                    $r->session()->flush();
                    return redirect('/');  //home page

                    //Apply promocode

                   //return redirect()->route('user.ApplyPromoCode'); 
        
                }
                //return redirect()->route('user.cart');
            }//if has session user
            /*
            else{
               
                return redirect()->route('user.cart');
               
            }
            */
            
        }//if has order

        if($r->has('signup'))
        { 
            $validatedData = $r->validate([
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'address' => 'required',
            'city' => 'required',
            //'zip' => 'required|numeric',
            'tel' => 'required|numeric',
            'pass' => 'required|min:5'
            ]);
            //dd($validatedData);
            $u=new User();
            $add=new Address();
            $add->area=$r->address;
            $add->city=$r->city;
            //$add->zip=$r->zip;
            $add->save();
            $add_id=$add->id;
            $u->full_name=$r->name;
            $u->email=$r->email;
            $u->password=$r->pass;
            $u->address_id=$add_id;
            $u->phone=$r->tel;
            //dd($u);
            $u->save();
            $user=User::find($u->id);
            Session::put('user',$user);
            
            return redirect()->route('user.cart');
        }

        
       
    }//end fun 



    public function apply_promocode(){

        $res = Product::all();
        $cat = Category::all();
        $result = PromoCode::all();


          //dd($cart); 
         return view('store.apply_promocode')
         ->with('products', $res)
         ->with("cat", $cat)
         ->with('promolist', $result);

    }

    public function apply_promocode_confirm(Request $request){


        //dd($request->user_promocode);

        //dd(Session::get('price'));
        //Session::get('price');
        $find_promocode = PromoCode::where('code','=',$request->user_promocode)
                        ->get()->first();

                        //dd($find_promocode->value);
        if($find_promocode){

            //after promocode
            $discount_price =(Session::get('price')) * (($find_promocode->value)/100);
            $new_price = (Session::get('price')) - ($discount_price);

            //dd($new_price);
            //then put new_price to session 
            Session::forget('price');
            Session::put('new_price',$new_price);
            //dd(Session::get('new_price'));

            return redirect()->route('user.cart');

        }                


    }    



    public function history(Request $r)
    {
        $res1= sale::where('user_id', session('user')->id)->get();  // or Session::get('user')->id
        if(!$res1)
        {
            return view('user.orderHistory')->with('all',[])
                 ->with('products',[])
                 ->with('sale',[]);
        }
        
        $cart=[];
        $product=[];
        $id=[];
        foreach($res1 as $r )
        {
             $totalCart = explode(',',$r->product_id);
             foreach($totalCart as $c)
             {
                $cart[]=array_prepend(explode(':',$c), $r->id);
                $a=explode(':',$c);
                $res = Product::find($a[0]);
                $product[]=$res;
             }
        }
        $res = Product::all();
        $cat = Category::all();
          //dd($cart); 
         return view('store.history')
         ->with('products', $res)
         ->with("cat", $cat)
         ->with('all',$cart)
         ->with('prods',$product)
         ->with('sale',$res1);
    }// end history


    //********* function rate product **************************************************/
    public function view_rate_product($id)
    {
        
        $res = Product::find($id);
        $res1 = Product::all();
        $cat=Category::find($res->category_id);
        //$colorList = explode(',',$res->colors);
        $colorList = $res->colors;

    	$cat = Category::all();
        // get image from db
        $image_name = json_decode($res->image_name);
        //dd($image_name);
        
         /**************** product rate **************/

         $avg_stars = DB::table('rate_product')->where('product_id','=',$id)->avg('user_rate');
         //$rate = Rate::where('product_id','=',$id)->get()->all();
         //$avg_rate_product = $rate->avg('user_rate');
         //dd($rate[0]->user_rate);
         $rate_after_round = round($avg_stars,1);  // to get number with one dicemal places
         //dd(number_format((float)$rate_after_round,0, '.', ''));
         //dd(format_rate); 

        return view('store.review_product')
            ->with('image_name',$image_name)
            ->with('product', $res)
            ->with('products', $res1)
            ->with('cat', $cat)
            ->with('colors',$colorList)
            ->with('rate',number_format((float)$rate_after_round,0, '.', ''));   

    }

        public function rate_product($id,Request $r)
        {
            //dd($r->has('rate')); // done
            $user_rate=new Rate();

            $user_rate->user_id = Session::get('user')->id;
            $user_rate->product_id =$id;
            $user_rate->user_rate =$r->rate;

            $user_rate->save();

            return redirect('/');


        }//end fun rate_product  



        public function about_us(){
            return view('about_us');
        }//end func

        public function order_returns(){
            return view('order_returns');
        }//end func

        public function terms_conditions(){

            return view('terms_conditions');
        }//end func
    
}//end class
